# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI=6
inherit cmake-utils
DESCRIPTION="High-speed, 3D-friendly, TightVNC-compatible remote desktop software."
HOMEPAGE="http://turbovnc.org/"
SRC_URI="mirror://sourceforge/turbovnc/${P}.tar.gz"
LICENSE="GPLv2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+java +viewer server"

DEPEND="media-libs/libjpeg-turbo[java?]"

RDEPEND="${DEPEND}
	viewer? ( !net-misc/tigervnc )
	server? ( !net-misc/tigervnc[server] )"

REQUIRED_USE="viewer? ( java )"

src_configure() {
	local mycmakeargs=(
		"-DTVNC_BUILDJAVA=$(usex java 1 0)"
		"-DTVNC_BUILDSERVER=$(usex server 1 0)"
		"-DTJPEG_JAR=/usr/share/classes/turbojpeg.jar"
		"-DTJPEG_JNILIBRARY=/usr/lib/libturbojpeg.so"
	)
	if use viewer || use server; then
		mycmakeargs+=( "-DTVNC_BUILDNATIVE=1" )
	else
		mycmakeargs+=( "-DTVNC_BUILDNATIVE=0" )
	fi
	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install
	# TODO: Really we need delete this?
	rm "${D}"usr/share/man/man1/Xserver.1
}
