# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/odb-cmake/odb.git"
	EGIT_BRANCH="dev"
	S="${WORKDIR}/${P}"
else
	if [[ ${PV} = *_beta* ]] ; then
		inherit git-r3
		EGIT_REPO_URI="https://gitlab.com/odb-cmake/odb.git"
		EGIT_COMMIT="v${PV/_beta/-b.}"
		echo $EGIT_COMMIT
#	else
#                SRC_URI="https://www.enigmail.net/download/source/${P}.tar.gz"
#                KEYWORDS="~alpha ~amd64 ~arm ~ppc ~ppc64 ~x86 ~x86-fbsd ~amd64-linux ~x86-linux"
	fi
#	S="${WORKDIR}/${PN}"
fi



#MY_PV="$(get_version_component_range 1).$(get_version_component_range 2)"

DESCRIPTION="Compiler and the system documentation for ODB ORM system for C++."
HOMEPAGE="http://www.codesynthesis.com/products/odb/"
#SRC_URI="http://www.codesynthesis.com/download/${PN}/$MY_PV/${P}.tar.bz2"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
DEPEND="dev-cpp/libcutl"
RDEPEND="${DEPEND}"

#TODO: add USE="doc"
