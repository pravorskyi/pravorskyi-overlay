# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

if [ ${PV} == "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/pravorskyi/${PN}.git"
	KEYWORDS=""
else
	inherit vcs-snapshot
	SRC_URI="https://gitlab.com/pravorskyi/${PN}/repository/archive.tar.bz2?ref=v${PV} -> ${P}.tar.bz2"
	KEYWORDS="~x86 ~amd64"
fi

DESCRIPTION="Kernel configs."
HOMEPAGE="https://gitlab.com/pravorskyi/kernel-configs"
LICENSE="GPL-3+"
SLOT="0"

src_install()
{
	insinto /usr/share/${PN}
	doins ${S}/*
}
