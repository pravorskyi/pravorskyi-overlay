# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit autotools versionator

MY_PV="$(get_version_component_range 1-2)"

DESCRIPTION="Codesynthesis' ODB Qt support library."
HOMEPAGE="http://www.codesynthesis.com/products/odb/"
SRC_URI="http://www.codesynthesis.com/download/odb/$MY_PV/${P}.tar.bz2"
LICENSE="|| ( GPL-2 ODB-FPL ODB-CPL )"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="static-libs threads qt5"

DEPEND="=dev-db/libodb-${MY_PV}*[static-libs?,threads=]
	!qt5? ( dev-qt/qtcore:4 )
	qt5? ( dev-qt/qtcore:5 )"

RDEPEND="${DEPEND}
	virtual/pkgconfig"

src_prepare() {
	local _pkg="$(use qt5 && echo Qt5Core || echo QtCore)"

	sed -i 's/libqt_lib_names=".*"/libqt_lib_names="'"${_pkg}"'"/g' m4/libqt.m4 || die "sed failed"
	sed -i 's/libqt_pkg_names=".*"/libqt_pkg_names="'"${_pkg}"'"/g' m4/libqt.m4 || die "sed failed"

	eautoreconf
	default
}

src_configure() {
	if use qt5; then
		# workaround; fixed in commit 776f2eed7c58a9dd8f262260bb9df309eeb1796b
		CXXFLAGS+=" -fPIC -std=c++11"
	fi

	econf CXXFLAGS="${CXXFLAGS}" \
		$(use_enable static-libs static) \
		$(use_enable threads)
}
