# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit eutils versionator

MY_PV="$(get_version_component_range 1-2)"

DESCRIPTION="Codesynthesis' ODB runtime library for accessing PostgreSQL."
HOMEPAGE="http://www.codesynthesis.com/products/odb/"
SRC_URI="http://www.codesynthesis.com/download/odb/$MY_PV/${P}.tar.bz2"
LICENSE="|| ( GPL-2 ODB-FPL ODB-CPL )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs threads"

DEPEND="=dev-db/libodb-${MY_PV}*[static-libs?,threads=]
	dev-db/postgresql:=[threads?]"

RDEPEND="${DEPEND}"

src_configure() {
	econf \
		$(use_enable static-libs static) \
		$(use_enable threads)
}
