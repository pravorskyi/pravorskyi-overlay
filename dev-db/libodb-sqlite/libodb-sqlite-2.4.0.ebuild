# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit eutils versionator

MY_PV="$(get_version_component_range 1).$(get_version_component_range 2)"

DESCRIPTION="SQLite ODB runtime library."
HOMEPAGE="http://www.codesynthesis.com/products/odb/"
SRC_URI="http://www.codesynthesis.com/download/odb/$MY_PV/${P}.tar.bz2"
LICENSE="GPLv2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-cpp/libodb
	dev-db/sqlite"

RDEPEND="${DEPEND}"
